package com.lid.viewpager.main.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lid.viewpager.R;

public class PagerFragment extends Fragment {
    String mContent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) // 在 PagerFragment 被Android系统创建的时候,回调此函数.
    {

        View view = inflater.inflate(R.layout.pager_fragment, container, false); // 装载 pager_fragment.xml

        TextView textView = view.findViewById(R.id.pager_fragment_textview); // 获取 TextView
        mContent = (String) getArguments().get("content"); // 取出 String (字符串)
        textView.setText(mContent); // 让 TextView 显示,刚刚取到的String
        return view;
    }
}
