package com.lid.viewpager.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lid.viewpager.R;
import com.lid.viewpager.main.fragment.PagerFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVp();
    }

    private void setVp() // 配置 ViewPager
    {

        final List<PagerFragment> list = new ArrayList<>(); // 创建一个可以盛放 PagerFragment 的 List

        for (int i = 0; i < 5; i++) // 创建五个PagerFragment塞进List
        {

            PagerFragment fragment = new PagerFragment();

            Bundle bundle = new Bundle();
            bundle.putString("content", "第" + i + "个Fragment"); // 给PagerFragment加点料,用Bundle传递个String给新创建的PagerFragment  让五个PagerFragment区分开来

            fragment.setArguments(bundle);
            list.add(fragment); // 塞进List
        }

        ViewPager vp = findViewById(R.id.view_pager);


        vp.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) // 这里用的匿名类 FragmentStatePagerAdapter
        {

            @Override
            public Fragment getItem(int position) // 按照 position 从 List 中依次取出 PagerFragment
            {
                return list.get(position);
            }

            @Override
            public int getCount() // 返回 List 中 PagerFragment 的总数
            {
                return list.size();
            }

        });
    }
}
